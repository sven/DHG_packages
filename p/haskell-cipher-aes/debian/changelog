haskell-cipher-aes (0.2.11-8) unstable; urgency=medium

  * Bump debhelper compat level to 10

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Sun, 30 Sep 2018 21:04:03 +0300

haskell-cipher-aes (0.2.11-7) unstable; urgency=medium

  [ Clint Adams ]
  * Set Rules-Requires-Root to no.

  [ Mathieu Trudel-Lapierre ]
  * Build with optimization disabled on armhf, to workaround SIGBUS in tests.
    (Closes: #901406)
    - this is not really a fix, but at least the produced binary is more sane.

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Fri, 22 Jun 2018 20:02:48 +0200

haskell-cipher-aes (0.2.11-6) unstable; urgency=medium

  [ Ilias Tsitsimpis ]
  * Change Priority to optional. Since Debian Policy version 4.0.1,
    priority extra has been deprecated.
  * Use the HTTPS form of the copyright-format URL
  * Modify d/watch and Source field in d/copyright to use HTTPS
  * Declare compliance with Debian policy 4.1.1
  * Use salsa.debian.org URLs in Vcs-{Browser,Git} fields

  [ Clint Adams ]
  * Bump to Standards-Version 4.1.4.

 -- Clint Adams <clint@debian.org>  Mon, 09 Apr 2018 20:04:43 -0400

haskell-cipher-aes (0.2.11-5) unstable; urgency=medium

  * Upload to unstable as part of GHC 8 transition.

 -- Clint Adams <clint@debian.org>  Thu, 27 Oct 2016 18:32:50 -0400

haskell-cipher-aes (0.2.11-4) experimental; urgency=medium

  [ Dmitry Bogatov ]
  * Use secure (https) uri in Vcs-Git field in 'debian/control'
  * Bump standards version to 3.9.8 (no changes needed)

  [ Clint Adams ]
  * Temporarily build-depend on ghc 8.

 -- Clint Adams <clint@debian.org>  Mon, 17 Oct 2016 16:17:23 -0400

haskell-cipher-aes (0.2.11-3) unstable; urgency=medium

  * Switch Vcs-Git/Vcs-Browser headers to new location.

 -- Clint Adams <clint@debian.org>  Thu, 03 Dec 2015 14:54:11 -0500

haskell-cipher-aes (0.2.11-2) experimental; urgency=medium

  * Bump standards-version to 3.9.6
  * Depend on haskell-devscripts >= 0.10 to ensure that this package
    builds against GHC in experimental

 -- Joachim Breitner <nomeata@debian.org>  Thu, 20 Aug 2015 10:27:10 +0200

haskell-cipher-aes (0.2.11-1) unstable; urgency=medium

  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Mon, 29 Jun 2015 21:38:30 +0200

haskell-cipher-aes (0.2.10-2) unstable; urgency=medium

  * Upload to unstable

 -- Joachim Breitner <nomeata@debian.org>  Mon, 27 Apr 2015 11:48:20 +0200

haskell-cipher-aes (0.2.10-1) experimental; urgency=medium

  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Sun, 05 Apr 2015 17:19:47 +0200

haskell-cipher-aes (0.2.8-2) experimental; urgency=medium

  * Depend on haskell-devscripts 0.9, found in experimental
  * Fix description

 -- Joachim Breitner <nomeata@debian.org>  Sun, 21 Dec 2014 15:06:54 +0100

haskell-cipher-aes (0.2.8-1) unstable; urgency=medium

  * New upstream version.

 -- Clint Adams <clint@debian.org>  Mon, 14 Jul 2014 15:00:33 -0400

haskell-cipher-aes (0.2.6-1) unstable; urgency=low

  * New upstream version.

 -- Clint Adams <clint@debian.org>  Mon, 07 Oct 2013 23:53:01 -0400

haskell-cipher-aes (0.2.5-1) unstable; urgency=low

  * Adjust watch file to new Hackage layout
  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Sat, 05 Oct 2013 17:27:26 +0200

haskell-cipher-aes (0.1.8-2) unstable; urgency=low

  * Enable compat level 9
  * Use substvars for Haskell description blurbs

 -- Joachim Breitner <nomeata@debian.org>  Fri, 24 May 2013 12:50:07 +0200

haskell-cipher-aes (0.1.8-1) experimental; urgency=low

  * New upstream release (Closes: 704662)

 -- Joachim Breitner <nomeata@debian.org>  Thu, 04 Apr 2013 11:05:30 +0200

haskell-cipher-aes (0.1.7-1) experimental; urgency=low

  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Wed, 13 Feb 2013 09:40:36 +0100

haskell-cipher-aes (0.1.2-1) experimental; urgency=low

  * Initial release.

 -- Clint Adams <clint@debian.org>  Sat, 17 Nov 2012 11:28:55 -0500
