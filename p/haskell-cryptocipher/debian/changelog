haskell-cryptocipher (0.6.2-9) unstable; urgency=medium

  [ Clint Adams ]
  * Set Rules-Requires-Root to no.

  [ Ilias Tsitsimpis ]
  * Bump debhelper compat level to 10

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Sun, 30 Sep 2018 21:05:09 +0300

haskell-cryptocipher (0.6.2-8) unstable; urgency=medium

  [ Ilias Tsitsimpis ]
  * Change Priority to optional. Since Debian Policy version 4.0.1,
    priority extra has been deprecated.
  * Use the HTTPS form of the copyright-format URL
  * Modify d/watch and Source field in d/copyright to use HTTPS
  * Declare compliance with Debian policy 4.1.1
  * Use salsa.debian.org URLs in Vcs-{Browser,Git} fields

  [ Clint Adams ]
  * Bump to Standards-Version 4.1.4.

 -- Clint Adams <clint@debian.org>  Mon, 09 Apr 2018 20:04:45 -0400

haskell-cryptocipher (0.6.2-7) unstable; urgency=medium

  * Upload to unstable as part of GHC 8 transition.

 -- Clint Adams <clint@debian.org>  Thu, 27 Oct 2016 18:33:04 -0400

haskell-cryptocipher (0.6.2-6) experimental; urgency=medium

  [ Dmitry Bogatov ]
  * Use secure (https) uri in Vcs-Git field in 'debian/control'
  * Bump standards version to 3.9.8 (no changes needed)
  * Convert `debian/copyright' to dep5 format

  [ Clint Adams ]
  * Temporarily build-depend on ghc 8.

 -- Clint Adams <clint@debian.org>  Mon, 17 Oct 2016 17:02:41 -0400

haskell-cryptocipher (0.6.2-5) unstable; urgency=medium

  * Switch Vcs-Git/Vcs-Browser headers to new location.

 -- Clint Adams <clint@debian.org>  Thu, 03 Dec 2015 14:54:16 -0500

haskell-cryptocipher (0.6.2-4) experimental; urgency=medium

  * Bump standards-version to 3.9.6
  * Depend on haskell-devscripts >= 0.10 to ensure that this package
    builds against GHC in experimental

 -- Joachim Breitner <nomeata@debian.org>  Thu, 20 Aug 2015 10:27:17 +0200

haskell-cryptocipher (0.6.2-3) unstable; urgency=medium

  * Upload to unstable

 -- Joachim Breitner <nomeata@debian.org>  Mon, 27 Apr 2015 11:48:41 +0200

haskell-cryptocipher (0.6.2-2) experimental; urgency=medium

  * Depend on haskell-devscripts 0.9, found in experimental

 -- Joachim Breitner <nomeata@debian.org>  Sat, 20 Dec 2014 17:10:00 +0100

haskell-cryptocipher (0.6.2-1) unstable; urgency=low

  * New upstream version.

 -- Clint Adams <clint@debian.org>  Sat, 07 Dec 2013 21:15:59 -0500

haskell-cryptocipher (0.6.1-1) unstable; urgency=low

  * Adjust watch file to new hackage layout
  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Sat, 05 Oct 2013 18:10:11 +0200

haskell-cryptocipher (0.5.2-1) unstable; urgency=low

  * New upstream version.

 -- Clint Adams <clint@debian.org>  Sat, 31 Aug 2013 12:13:22 -0700

haskell-cryptocipher (0.5.0-1) unstable; urgency=low

  * Enable compat level 9
  * Use substvars for Haskell description blurbs
  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Sat, 25 May 2013 18:09:59 +0200

haskell-cryptocipher (0.3.6-1) experimental; urgency=low

  * New upstream version.

 -- Clint Adams <clint@debian.org>  Sat, 17 Nov 2012 12:09:36 -0500

haskell-cryptocipher (0.3.5-2) experimental; urgency=low

  * Enable AES-NI support, thanks to Nuutti Kotivuori for the patch (Closes:
    #686066)
  * Depend on haskell-devscripts 0.8.13 to ensure this package is built
    against experimental
  * Bump standards version, no change

 -- Joachim Breitner <nomeata@debian.org>  Thu, 18 Oct 2012 10:25:09 +0200

haskell-cryptocipher (0.3.5-1) unstable; urgency=low

  * New upstream release, includes our patch.

 -- Joachim Breitner <nomeata@debian.org>  Fri, 15 Jun 2012 10:18:36 +0200

haskell-cryptocipher (0.3.3-3) unstable; urgency=low

  * Upload to unstable

 -- Joachim Breitner <nomeata@debian.org>  Fri, 15 Jun 2012 09:24:38 +0200

haskell-cryptocipher (0.3.3-2) experimental; urgency=low

  * haskell-cryptocipher on BigEndian architectures (Closes: #674811)

 -- Joachim Breitner <nomeata@debian.org>  Fri, 15 Jun 2012 09:24:29 +0200

haskell-cryptocipher (0.3.3-1) unstable; urgency=low

  * New upstream version.
  * Add missing build dependency on cryptohash (for test suite).
    closes: #673275.

 -- Clint Adams <clint@debian.org>  Fri, 18 May 2012 12:38:57 -0400

haskell-cryptocipher (0.3.2-1) unstable; urgency=low

  * New upstream version.
  * Enable test suite.

 -- Clint Adams <clint@debian.org>  Tue, 15 May 2012 15:29:22 -0400

haskell-cryptocipher (0.3.0-3) unstable; urgency=low

  * Bump to Standards-Version 3.9.3.

 -- Clint Adams <clint@debian.org>  Tue, 01 May 2012 15:09:50 -0400

haskell-cryptocipher (0.3.0-2) unstable; urgency=low

  * Sourceful upload to rebuild documentation package.

 -- Clint Adams <clint@debian.org>  Fri, 10 Feb 2012 23:33:31 -0500

haskell-cryptocipher (0.3.0-1) unstable; urgency=low

  * New upstream version.

 -- Clint Adams <clint@debian.org>  Sun, 25 Dec 2011 21:09:19 -0500

haskell-cryptocipher (0.2.14-1) unstable; urgency=low

  * New upstream version.

 -- Clint Adams <clint@debian.org>  Sat, 27 Aug 2011 22:12:09 -0400

haskell-cryptocipher (0.2.12-1) unstable; urgency=low

  * Initial release.

 -- Clint Adams <clint@debian.org>  Sat, 04 Jun 2011 12:38:41 -0400
