Source: haskell-descriptive
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Joachim Breitner <nomeata@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8.4.3),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-aeson-dev,
 libghc-aeson-dev (>= 0.7.0.5),
 libghc-aeson-prof,
 libghc-bifunctors-dev,
 libghc-bifunctors-prof,
 libghc-hspec-dev,
 libghc-hunit-dev,
 libghc-scientific-dev (>= 0.3.2),
 libghc-scientific-prof,
 libghc-vector-dev,
 libghc-vector-prof,
Build-Depends-Indep:
 ghc-doc,
 libghc-aeson-doc,
 libghc-bifunctors-doc,
 libghc-scientific-doc,
 libghc-vector-doc,
Standards-Version: 4.1.4
Homepage: https://github.com/chrisdone/descriptive
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-descriptive
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-descriptive]
X-Description: Self-describing consumers/parsers; forms, cmd-line args, JSON, etc.
 There are a variety of Haskell libraries which are implementable
 through a common interface: self-describing parsers:
 .
  * A formlet is a self-describing parser.
  * A regular old text parser can be self-describing.
  * A command-line options parser is a self-describing parser.
  * A MUD command set is a self-describing parser.
  * A JSON API can be a self-describing parser.

Package: libghc-descriptive-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-descriptive-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-descriptive-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
