Source: haskell-binary-orphans
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Ilias Tsitsimpis <iliastsi@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8.4.3),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-aeson-dev (>= 0.7.0.6),
 libghc-aeson-dev (<< 1.5),
 libghc-aeson-prof,
 libghc-case-insensitive-dev (<< 1.2.1),
 libghc-case-insensitive-dev (>= 1.2.0.4),
 libghc-case-insensitive-prof,
 libghc-hashable-dev (>= 1.2.3.3),
 libghc-hashable-dev (<< 1.3),
 libghc-hashable-prof,
 libghc-scientific-dev (<< 0.4),
 libghc-scientific-dev (>= 0.3.3.8),
 libghc-scientific-prof,
 libghc-semigroups-dev (>= 0.16.2.2),
 libghc-semigroups-dev (<< 0.18.6),
 libghc-semigroups-prof,
 libghc-tagged-dev (>= 0.7.3),
 libghc-tagged-dev (<< 0.8.7),
 libghc-tagged-prof,
 libghc-text-binary-dev (<< 0.3),
 libghc-text-binary-dev (>= 0.1.0),
 libghc-text-binary-prof,
 libghc-unordered-containers-dev (<< 0.3),
 libghc-unordered-containers-dev (>= 0.2.5.1),
 libghc-unordered-containers-prof,
 libghc-vector-dev (>= 0.10.12.3),
 libghc-vector-dev (<< 0.13),
 libghc-vector-prof,
 libghc-vector-binary-instances-dev (<< 0.3),
 libghc-vector-binary-instances-dev (>= 0.2.1.0),
 libghc-vector-binary-instances-prof,
 libghc-quickcheck2-dev (>= 2.10),
 libghc-quickcheck2-dev (<< 2.13),
 libghc-quickcheck-instances-dev (>= 0.3.16),
 libghc-quickcheck-instances-dev (<< 0.4),
 libghc-tasty-dev (>= 0.10.1.2),
 libghc-tasty-dev (<< 1.3),
 libghc-tasty-quickcheck-dev (>= 0.8.3.2),
 libghc-tasty-quickcheck-dev (<< 0.11),
Build-Depends-Indep: ghc-doc,
 libghc-aeson-doc,
 libghc-case-insensitive-doc,
 libghc-hashable-doc,
 libghc-scientific-doc,
 libghc-semigroups-doc,
 libghc-tagged-doc,
 libghc-text-binary-doc,
 libghc-unordered-containers-doc,
 libghc-vector-binary-instances-doc,
 libghc-vector-doc,
Standards-Version: 4.1.4
Homepage: https://github.com/phadej/binary-orphans#readme
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-binary-orphans
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-binary-orphans]
X-Description: orphan instances for binary
 `binary-orphans` defines orphan instances for types in some popular packages,
 in particular:
 .
  * aeson
  * unordered-containers (HashMap, HashSet)
  * scientific
  * tagged
  * base (newtypes from Data.Monoid)
  * time

Package: libghc-binary-orphans-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-binary-orphans-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-binary-orphans-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
