Source: haskell-text-show
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Clint Adams <clint@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8.4.3),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-base-compat-batteries-dev (>= 0.10),
 libghc-base-compat-batteries-dev (<< 0.11),
 libghc-base-compat-batteries-prof,
 libghc-bifunctors-dev (>= 5.1),
 libghc-bifunctors-dev (<< 6),
 libghc-bifunctors-prof,
 libghc-contravariant-dev (<< 2),
 libghc-contravariant-dev (>= 0.5),
 libghc-contravariant-prof,
 libghc-deriving-compat-dev (<< 1),
 libghc-deriving-compat-dev (>= 0.3),
 libghc-generic-deriving-dev (<< 2),
 libghc-generic-deriving-dev (>= 1.11),
 libghc-generic-deriving-prof,
 libghc-hspec-dev (>= 2),
 libghc-hspec-dev (<< 3),
 libghc-quickcheck-instances-dev (>= 0.1),
 libghc-quickcheck-instances-dev (<< 0.4),
 libghc-semigroups-dev (>= 0.17),
 libghc-semigroups-dev (<< 1),
 libghc-semigroups-prof,
 libghc-tagged-dev (>= 0.8.3),
 libghc-tagged-prof,
 libghc-th-abstraction-dev (>= 0.2.2),
 libghc-th-abstraction-dev (<< 1),
 libghc-th-abstraction-prof,
 libghc-th-lift-dev (>= 0.7.6),
 libghc-th-lift-dev (<< 1),
 libghc-th-lift-prof,
 libghc-transformers-compat-dev (>= 0.5),
 libghc-transformers-compat-dev (<< 1),
 libghc-transformers-compat-prof,
 libghc-void-dev (>= 0.5),
 libghc-void-dev (<< 1),
 libghc-void-prof,
 libghc-quickcheck2-dev (>= 2.10),
 libghc-quickcheck2-dev (<< 2.13),
 libghc-deriving-compat-dev (>= 0.3.4),
 libghc-deriving-compat-dev (<< 1),
 libghc-hspec-dev (>= 2),
 libghc-hspec-dev (<< 3),
 libghc-quickcheck-instances-dev (>= 0.3.18),
 libghc-quickcheck-instances-dev (<< 0.4),
Build-Depends-Indep: ghc-doc,
 libghc-base-compat-batteries-doc,
 libghc-bifunctors-doc,
 libghc-contravariant-doc,
 libghc-generic-deriving-doc,
 libghc-semigroups-doc,
 libghc-tagged-doc,
 libghc-th-abstraction-doc,
 libghc-th-lift-doc,
 libghc-transformers-compat-doc,
 libghc-void-doc,
Standards-Version: 4.1.4
Homepage: https://github.com/RyanGlScott/text-show
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-text-show
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-text-show]
X-Description: efficient conversion of values into Text
 A replacement for the Show typeclass intended for use with Text
 instead of Strings, in the spirit of bytestring-show.

Package: libghc-text-show-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-text-show-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-text-show-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
