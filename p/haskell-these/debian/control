Source: haskell-these
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Ilias Tsitsimpis <iliastsi@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8.4.3),
 ghc-prof,
 haskell-devscripts (>= 0.15),
 libghc-quickcheck2-dev (>= 2.12.6.1),
 libghc-quickcheck2-dev (<< 2.13),
 libghc-quickcheck2-prof,
 libghc-aeson-dev (>= 1.4.2.0),
 libghc-aeson-dev (<< 1.5),
 libghc-aeson-prof,
 libghc-base-compat-dev (>= 0.10.5),
 libghc-base-compat-dev (<< 0.11),
 libghc-base-compat-prof,
 libghc-bifunctors-dev (>= 5.5.3),
 libghc-bifunctors-dev (<< 5.6),
 libghc-bifunctors-prof,
 libghc-data-default-class-dev (>= 0.1.2.0),
 libghc-hashable-dev (>= 1.2.7.0),
 libghc-hashable-dev (<< 1.3),
 libghc-hashable-prof,
 libghc-keys-dev (>= 3.12.1),
 libghc-keys-dev (<< 3.13),
 libghc-keys-prof,
 libghc-lens-dev (>= 4.17),
 libghc-lens-dev (<< 4.18),
 libghc-lens-prof,
 libghc-semigroupoids-dev (>= 5.3.1),
 libghc-semigroupoids-dev (<< 5.4),
 libghc-semigroupoids-prof,
 libghc-transformers-compat-dev (>= 0.6.2),
 libghc-transformers-compat-dev (<< 0.7),
 libghc-transformers-compat-prof,
 libghc-unordered-containers-dev (>= 0.2.8.0),
 libghc-unordered-containers-dev (<< 0.3),
 libghc-unordered-containers-prof,
 libghc-vector-dev (>= 0.12.0.2),
 libghc-vector-dev (<< 0.13),
 libghc-vector-prof,
 libghc-vector-instances-dev (>= 3.4),
 libghc-vector-instances-dev (<< 3.5),
 libghc-vector-instances-prof,
 libghc-quickcheck2-dev,
 libghc-aeson-dev,
 libghc-base-compat-dev,
 libghc-bifunctors-dev,
 libghc-hashable-dev,
 libghc-lens-dev,
 libghc-quickcheck-instances-dev (>= 0.3.15),
 libghc-quickcheck-instances-dev (<< 0.4),
 libghc-quickcheck-instances-prof,
 libghc-tasty-dev (>= 1.2),
 libghc-tasty-dev (<< 1.3),
 libghc-tasty-prof,
 libghc-tasty-quickcheck-dev (>= 0.10),
 libghc-tasty-quickcheck-dev (<< 0.11),
 libghc-tasty-quickcheck-prof,
 libghc-unordered-containers-dev,
 libghc-vector-dev,
 libghc-aeson-doc,
 libghc-base-compat-doc,
 libghc-bifunctors-doc,
 libghc-data-default-class-doc,
 libghc-hashable-doc,
 libghc-keys-doc,
 libghc-lens-doc,
 libghc-semigroupoids-doc,
 libghc-vector-doc,
 libghc-vector-instances-doc,
Standards-Version: 4.4.0
Homepage: https://github.com/isomorphism/these
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-these
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-these]
X-Description: either-or-both data type
 Package provides a data type `These a b' which can hold a value of either
 type or values of each type. This is usually thought of as an "inclusive or"
 type (contrasting `Either a b' as "exclusive or") or as an "outer join" type
 (contrasting `(a, b)' as "inner join").
 .
 The major use case of this is provided by the Align class, representing a
 generalized notion of "zipping with padding" that combines structures without
 truncating to the size of the smaller input.
 .
 Also included is ChronicleT, a monad transformer based on the Monad instance
 for `These a', along with the usual monad transformer bells and whistles.

Package: libghc-these-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-these-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-these-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
