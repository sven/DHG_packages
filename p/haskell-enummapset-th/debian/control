Source: haskell-enummapset-th
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders: Clint Adams <clint@debian.org>
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends: debhelper (>= 10),
 haskell-devscripts-minimal | haskell-devscripts (>= 0.8),
 cdbs,
 ghc,
 ghc-prof,
 libghc-semigroups-dev,
 libghc-semigroups-prof,
Build-Depends-Indep: ghc-doc,
Standards-Version: 4.1.4
Homepage: https://github.com/tsurucapital/enummapset-th
X-Description: TH-generated EnumSet/EnumMap wrappers around IntSet/IntMap
 This package wraps IntSet and IntMap from containers, and provides
 fast sets and maps keyed on any data type with a well-behaved Enum
 instance. Useful for derived Enums, newtype'd Ints, or any data type
 that can be packed into an Int: just implement fromEnum and toEnum.
 .
 The boilerplate is generated using Template Haskell, so unlike
 enummapset it's easier to maintain and keep up-to-date with
 containers. On the downside, it's less portable.
 .
 Note that "Data.EnumMap.Lazy" and "Data.EnumMap.Strict" provide distinct
 newtype wrappers, and their respective 'Functor' instances behave as
 expected, unlike that of IntMap which is alway lazy.

Package: libghc-enummapset-th-dev
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-enummapset-th-prof
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-enummapset-th-doc
Architecture: all
Section: doc
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
