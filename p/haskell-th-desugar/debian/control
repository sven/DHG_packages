Source: haskell-th-desugar
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders: Clint Adams <clint@debian.org>
Priority: optional
Section: haskell
Build-Depends: debhelper (>= 10),
 haskell-devscripts-minimal | haskell-devscripts (>= 0.8),
 cdbs,
 ghc,
 ghc-prof,
 libghc-syb-dev (>= 0.4),
 libghc-syb-prof (>= 0.4),
 libghc-th-expand-syns-dev (>= 0.3.0.6),
 libghc-th-expand-syns-prof (>= 0.3.0.6),
 libghc-th-lift-dev (>= 0.6.1),
 libghc-th-lift-prof (>= 0.6.1),
 libghc-th-orphans-dev (>= 0.9.1),
 libghc-th-orphans-prof (>= 0.9.1),
 libghc-hunit-dev (>= 1.2),
 libghc-hunit-prof (>= 1.2),
 libghc-hspec-dev (>= 1.3),
 libghc-hspec-prof (>= 1.3),
Build-Depends-Indep: ghc-doc,
 libghc-syb-doc,
 libghc-th-expand-syns-doc,
 libghc-th-lift-doc,
 libghc-th-orphans-doc,
Standards-Version: 4.2.1
Homepage: https://github.com/goldfirere/th-desugar
X-Description: functions to desugar Template Haskell
 This package provides the Language.Haskell.TH.Desugar module, which desugars
 Template Haskell's rich encoding of Haskell syntax into a simpler encoding.
 This desugaring discards surface syntax information (such as the use of infix
 operators) but retains the original meaning of the TH code. The intended use
 of this package is as a preprocessor for more advanced code manipulation
 tools. Note that the input to any of the ds... functions should be produced
 from a TH quote, using the syntax [| ... |]. If the input to these functions
 is a hand-coded TH syntax tree, the results may be unpredictable. In
 particular, it is likely that promoted datatypes will not work as expected.

Package: libghc-th-desugar-dev
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-th-desugar-prof
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-th-desugar-doc
Architecture: all
Section: doc
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
