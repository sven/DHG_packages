Source: haskell-hledger-lib
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Clint Adams <clint@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8.4.3),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-decimal-dev,
 libghc-decimal-prof,
 libghc-glob-dev (>= 0.9),
 libghc-glob-prof,
 libghc-ansi-terminal-dev (>= 0.6.2.3),
 libghc-ansi-terminal-prof,
 libghc-base-compat-batteries-dev (>= 0.10.1),
 libghc-base-compat-batteries-dev (<< 0.11),
 libghc-base-compat-batteries-prof,
 libghc-blaze-markup-dev (>= 0.5.1),
 libghc-blaze-markup-prof,
 libghc-call-stack-dev,
 libghc-call-stack-prof,
 libghc-cassava-dev,
 libghc-cassava-prof,
 libghc-cassava-megaparsec-dev,
 libghc-cassava-megaparsec-prof,
 libghc-cmdargs-dev (>= 0.10),
 libghc-cmdargs-prof,
 libghc-data-default-dev (>= 0.5),
 libghc-data-default-prof,
 libghc-easytest-dev (>= 0.2.1),
 libghc-easytest-dev (<< 0.3),
 libghc-easytest-prof,
 libghc-extra-dev,
 libghc-extra-prof,
 libghc-file-embed-dev (>= 0.0.10),
 libghc-file-embed-prof,
 libghc-hashtables-dev (>= 1.2.3.1),
 libghc-hashtables-prof,
 libghc-megaparsec-dev (>= 7.0.0),
 libghc-megaparsec-dev (<< 8),
 libghc-megaparsec-prof,
 libghc-old-time-dev,
 libghc-old-time-prof,
 libghc-parser-combinators-dev (>= 0.4.0),
 libghc-parser-combinators-prof,
 libghc-pretty-show-dev (>= 1.6.4),
 libghc-pretty-show-prof,
 libghc-regex-tdfa-dev,
 libghc-regex-tdfa-prof,
 libghc-safe-dev (>= 0.2),
 libghc-safe-prof,
 libghc-split-dev (>= 0.1),
 libghc-split-prof,
 libghc-tabular-dev (>= 0.2),
 libghc-tabular-prof,
 libghc-uglymemo-dev,
 libghc-uglymemo-prof,
 libghc-utf8-string-dev (>= 0.3.5),
 libghc-utf8-string-prof,
 libghc-doctest-dev (>= 0.16) <!nocheck>,
Build-Depends-Indep: ghc-doc,
 libghc-decimal-doc,
 libghc-glob-doc,
 libghc-ansi-terminal-doc,
 libghc-base-compat-batteries-doc,
 libghc-blaze-markup-doc,
 libghc-call-stack-doc,
 libghc-cassava-doc,
 libghc-cassava-megaparsec-doc,
 libghc-cmdargs-doc,
 libghc-data-default-doc,
 libghc-easytest-doc,
 libghc-extra-doc,
 libghc-file-embed-doc,
 libghc-hashtables-doc,
 libghc-megaparsec-doc,
 libghc-old-time-doc,
 libghc-parser-combinators-doc,
 libghc-pretty-show-doc,
 libghc-regex-tdfa-doc,
 libghc-safe-doc,
 libghc-split-doc,
 libghc-tabular-doc,
 libghc-uglymemo-doc,
 libghc-utf8-string-doc,
Standards-Version: 4.4.1
Homepage: https://hledger.org
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-hledger-lib
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-hledger-lib]

Package: libghc-hledger-lib-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: core data types, parsers and utilities for the hledger accounting tool${haskell:ShortBlurb}
 hledger is a haskell port and friendly fork of John Wiegley's ledger
 accounting tool. This package provides core data types, parsers and
 utilities used by the hledger tools. It also aims to be a useful
 library for building h/ledger-compatible tools or unrelated financial
 apps in Haskell.
 .
 ${haskell:Blurb}

Package: libghc-hledger-lib-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: core data types, parsers and utilities for hledger${haskell:ShortBlurb}
 hledger is a haskell port and friendly fork of John Wiegley's ledger
 accounting tool. This package provides core data types, parsers and
 utilities used by the hledger tools. It also aims to be a useful
 library for building h/ledger-compatible tools or unrelated financial
 apps in Haskell.
 .
 ${haskell:Blurb}

Package: libghc-hledger-lib-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: core data types, parsers and utilities for hledger${haskell:ShortBlurb}
 hledger is a haskell port and friendly fork of John Wiegley's ledger
 accounting tool. This package provides core data types, parsers and
 utilities used by the hledger tools. It also aims to be a useful
 library for building h/ledger-compatible tools or unrelated financial
 apps in Haskell.
 .
 ${haskell:Blurb}
