Source: haskell-gnutls
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders: Clint Adams <clint@debian.org>
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-monads-tf-dev (<< 0.2),
 libghc-monads-tf-dev (>= 0.1),
 libghc-monads-tf-prof,
 libgnutls28-dev | libgnutls-dev,
 pkg-config,
Build-Depends-Indep: ghc-doc, libghc-monads-tf-doc
Standards-Version: 4.1.4
Homepage: https://john-millikin.com/software/haskell-gnutls/
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-gnutls
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-gnutls]

Package: libghc-gnutls-dev
Architecture: any
Depends:
 libgnutls28-dev | libgnutls-dev,
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends: ${haskell:Recommends}
Suggests: ${haskell:Suggests}
Provides: ${haskell:Provides}
Description: bindings for GNU TLS
 This library provides Haskell bindings for gnutls.
 .
 This package contains the normal library files.

Package: libghc-gnutls-prof
Architecture: any
Depends: ${haskell:Depends}, ${misc:Depends}
Recommends: ${haskell:Recommends}
Suggests: ${haskell:Suggests}
Provides: ${haskell:Provides}
Description: bindings for GNU TLS; profiling libraries
 This library provides Haskell bindings for gnutls.
 .
 This package contains the libraries compiled with profiling enabled.

Package: libghc-gnutls-doc
Architecture: all
Section: doc
Depends: ${haskell:Depends}, ${misc:Depends}
Recommends: ${haskell:Recommends}
Suggests: ${haskell:Suggests}
Description: bindings for GNU TLS; documentation
 This library provides Haskell bindings for gnutls.
 .
 This package contains the documentation files.
