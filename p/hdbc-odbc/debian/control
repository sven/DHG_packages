Source: hdbc-odbc
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 John Goerzen <jgoerzen@complete.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 cpphs,
 debhelper (>= 10),
 ghc (>= 8.4.3),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-hdbc-dev (>= 2.1.0),
 libghc-hdbc-prof,
 libghc-concurrent-extra-dev (>= 0.7.0.8),
 libghc-concurrent-extra-prof,
 libghc-utf8-string-dev,
 libghc-utf8-string-prof,
 libodbc-dev,
 libpthread-dev,
Build-Depends-Indep: ghc-doc,
 libghc-hdbc-doc,
 libghc-concurrent-extra-doc,
 libghc-time-doc,
Standards-Version: 4.1.4
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/hdbc-odbc
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/hdbc-odbc]

Package: libghc-hdbc-odbc-dev
Architecture: any
Depends:
 unixodbc-dev (>= 2.2.11),
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: unixODBC HDBC (Haskell Database Connectivity) Driver for GHC
 HDBC provides an abstraction layer between Haskell programs and SQL
 relational databases. This lets you write database code once, in
 Haskell, and have it work with any number of backend SQL databases
 (MySQL, Oracle, PostgreSQL, ODBC-compliant databases, etc.)
 .
 This package provides the ODBC database driver for HDBC under GHC.

Package: libghc-hdbc-odbc-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: unixODBC Haskell Database Connectivity Driver; profiling libraries
 HDBC provides an abstraction layer between Haskell programs and SQL
 relational databases. This lets you write database code once, in
 Haskell, and have it work with any number of backend SQL databases
 (MySQL, Oracle, PostgreSQL, ODBC-compliant databases, etc.)
 .
 This package provides the ODBC database driver for HDBC under GHC compiled for
 profiling.

Package: libghc-hdbc-odbc-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 haskell-hdbc-odbc-doc (<< 2.2.3.0-2),
Provides:
 haskell-hdbc-odbc-doc,
 ${haskell:Provides},
Replaces:
 haskell-hdbc-odbc-doc (<< 2.2.3.0-2),
Description: unixODBC HDBC (Haskell Database Connectivity) Documentation
 HDBC provides an abstraction layer between Haskell programs and SQL
 relational databases. This lets you write database code once, in
 Haskell, and have it work with any number of backend SQL databases
 (MySQL, Oracle, PostgreSQL, ODBC-compliant databases, etc.)
 .
 This package provides documentation for the ODBC database driver for HDBC
 under GHC.
