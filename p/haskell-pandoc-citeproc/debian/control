Source: haskell-pandoc-citeproc
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Clint Adams <clint@debian.org>,
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends:
 cdbs,
 debhelper (>= 10),
 ghc (>= 8.4.3),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-aeson-dev (>= 0.7),
 libghc-aeson-dev (<< 1.5),
 libghc-aeson-prof,
 libghc-base-compat-dev (>= 0.9),
 libghc-base-compat-prof,
 libghc-data-default-dev,
 libghc-data-default-prof,
 libghc-hs-bibutils-dev (>= 6.4),
 libghc-hs-bibutils-prof,
 libghc-old-locale-dev,
 libghc-old-locale-prof,
 libghc-pandoc-dev (>= 1.16),
 libghc-pandoc-dev (<< 2.6),
 libghc-pandoc-prof,
 libghc-pandoc-types-dev (>= 1.17.3),
 libghc-pandoc-types-dev (<< 1.18),
 libghc-pandoc-types-prof,
 libghc-setenv-dev (<< 0.2),
 libghc-setenv-dev (>= 0.1),
 libghc-setenv-prof,
 libghc-split-dev,
 libghc-split-prof,
 libghc-syb-dev,
 libghc-syb-prof,
 libghc-tagsoup-dev,
 libghc-tagsoup-prof,
 libghc-temporary-dev (>= 1.1),
 libghc-text-icu-dev,
 libghc-text-icu-prof,
 libghc-unordered-containers-dev (<< 0.3),
 libghc-unordered-containers-dev (>= 0.2),
 libghc-unordered-containers-prof,
 libghc-vector-dev,
 libghc-vector-dev (>= 0.10),
 libghc-vector-prof,
 libghc-xml-conduit-dev (>= 1.2),
 libghc-xml-conduit-dev (<< 1.9),
 libghc-xml-conduit-prof,
 libghc-yaml-dev (>= 0.8.32),
 libghc-yaml-prof,
Build-Depends-Indep:
 ghc-doc,
 libghc-aeson-doc,
 libghc-base-compat-doc,
 libghc-data-default-doc,
 libghc-hs-bibutils-doc,
 libghc-pandoc-doc,
 libghc-pandoc-types-doc,
 libghc-setenv-doc,
 libghc-split-doc,
 libghc-syb-doc,
 libghc-tagsoup-doc,
 libghc-text-icu-doc,
 libghc-unordered-containers-doc,
 libghc-vector-doc,
 libghc-xml-conduit-doc,
 libghc-yaml-doc,
Standards-Version: 4.1.4
Homepage: https://github.com/jgm/pandoc-citeproc
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-pandoc-citeproc
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-pandoc-citeproc]
X-Description: support for using pandoc with citeproc
 The pandoc-citeproc library exports functions for using the citeproc
 system with pandoc. It relies on citeproc-hs, a library for rendering
 bibliographic reference citations into a variety of styles using a
 macro language called Citation Style Language (CSL). More details on
 CSL can be found here: http://citationstyles.org/ .

Package: libghc-pandoc-citeproc-dev
Architecture: any
Depends:
 libghc-pandoc-citeproc-data (>= ${source:Version}),
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-pandoc-citeproc-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-pandoc-citeproc-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-pandoc-citeproc-data
Architecture: all
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Breaks:
 pandoc (<< 1.11.1-3),
Description: Pandoc support for Citation Style Language - data files
 pandoc-citeproc is a Haskell implementation of the Citation Style Language
 (CSL).
 .
 pandoc-citeproc adds to pandoc, the famous Haskell text processing tool, a
 Bibtex like citation and bibliographic formatting and generation
 facility.
 .
 CSL is an XML language for specifying citation and bibliographic
 formatting, similar in principle to BibTeX .bst files or the binary
 style files in commercial products like Endnote or Reference Manager.
 .
 This package contains the data files needed to use the library.

Package: pandoc-citeproc
Architecture: any
Depends:
 libghc-pandoc-citeproc-data (>= ${source:Version}),
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: Pandoc support for Citation Style Language - tools
 pandoc-citeproc is a Haskell implementation of the Citation Style Language
 (CSL).
 .
 This package also contains an executable: pandoc-citeproc, which works as a
 pandoc filter (pandoc >= 1.12), and also has a mode for converting
 bibliographic databases a YAML format suitable for inclusion in pandoc YAML
 metadata.
